import discord
import os
import re
import Player
import datetime
import pickle
import traceback
from dateutil import parser

client = discord.Client()

# Minimal time for 2 sessions to be considered different (in seconds)
max_time_between_sessions = 299

#debuggex.com
channel_pattern = '.*-fin-*[0-9]([0-9])*h([0-9][0-9])*'
trigger_pattern = '^((je suis|on est) ){0,1}((([\+-]{0,1}[0-9]+)( (ok|dispo|chaud)){0,1})|(ok|dispo|chaud){0,1})(( (pour|à|a)){0,1}( [0-9]{1,2}:[0-9]{2}| [0-9]{1,2}h([^0-9]|$)| [0-9]{1,2}h[0-9]{2})){0,1}( \(([0-9]+(\, *[0-9]+)*)\)){0,1}.*$'

def get_token():
    token = None
    with open(".token", "r") as f:
        token = f.read()
    return token

@client.event
async def on_channel_create(channel):
    p = re.compile(channel_pattern)
    m = p.match(channel.name)
    if m is None:
        return
    await summon_bot(channel)

async def summon_bot(channel):
    # Load data
    if not os.path.exists('channels'):
        channels = {}
    else:
        channels = load('channels')

    channel_id = channel.id

    # Already summoned...
    if channel_id in channels:
        return
    
    first_content = '''
    [This message will automatically update with participating players list]
    Example of messages recognized by the bot :
      * Je suis dispo pour 12h30 je suis pas loin
      * On est 3 pour 12h
      * Chaud pour 12h15 (34)
      * 2 ok à 13h15
      * +1 13h15
      * -1 13h15
    A recognized message will have a checkmark when recognized by the bot.
    In case of bug, ping @tama
    '''
    msg = await client.send_message(content=first_content, destination=channel)
    channels[channel_id] = msg
    save('channels', channels)
    channel_data = { "player_list": {}, "sessions_list": [] }
    save(channel_id, channel_data)
        
@client.event
async def on_ready():
    print("Connected !")

@client.event
async def on_message(message):
    words = message.content.split()

    if len(words) > 1:
        # @rsvp-raid-bot commands :
        # summon -> summon the bot to the current channel if it is not there
        if (words[0] == '<@' + str(client.user.id) + '>') and words[1] == 'summon':
            await summon_bot(message.channel)

    # Check if the bot is watching this channel
    if not os.path.exists('channels'):
        channels = {}
    else:
        channels = load('channels')

    if message.channel.id not in channels:
        return

    pattern = re.compile(trigger_pattern, re.IGNORECASE)
    m = pattern.match(message.content)

    if m is not None:
        try:
            answer = update_list(message, m)
            if answer is None:
                return

            msg = channels[message.channel.id]
            await client.edit_message(msg, new_content=answer)

            # Add a feedback to the message which triggered the bot
            await client.add_reaction(message, u"\u2705") # White check mark
            
        except Exception as inst:
            print(type(inst))
            print(inst)
            traceback.print_exc()
                
def update_list(message, m):
    # Called when a message posted on a channel triggered the bot
    # Format of valid messages :
    # +[number] pour XX:XX
    # +[number] à XX:XX
    # +[number] pour XXh
    # +[number] à XX:XX
    # +[number] XX:XX
    # -[number] XX:XX

    # Predicates :
    #   The message posted is valid (checked before in on_message)
    #   The bot is already watching the channel on which the message was posted
    channel_data = load(message.channel.id)
    
    player_list = channel_data["player_list"]
    sessions_list = channel_data["sessions_list"]

    # ---
    # Extract informations from message received
    # ---
    g = m.groups()
    print(g)
    
    if g[4] is not None:
        nb = int(g[4])
    elif g[5] is not None or g[7] is not None:
        nb = 1
    else:
        # Missing information about number of players
        return None

    # hour
    notime = g[11] is None
    if notime is False:
        hour = g[11].strip()
    else:
        hour = "00:00"
        
    # lv list
    lvs = []
    if g[14] is not None:
        lvs = [int(lv.strip()) for lv in g[14].split(',')]

    print("{0} @ {1} ({2})".format(nb, hour, lvs))
    userRoles = discord.utils.get(message.server.members, name=message.author.name).roles
    p = Player.Player(userRoles, hour, nb, 0, message.content, lvs, notime)

    # Invalid time
    if p.from_time is None:
        return None

    if notime is True:
        # No time given, goes to the pool of players available
        session_key = '?'
    else:
        # Find existing session from hour given
        session = None
        for s in sessions_list:
            if s > p.from_time:
                delta = s - p.from_time
            else:
                delta = p.from_time - s

            if delta.seconds <= max_time_between_sessions:
                session = s
                break

        if session is None:
            session_key = p.from_time.strftime("%H:%M")
        else:
            session_key = session.strftime("%H:%M")

    if session_key not in player_list:
        player_list[session_key] = {}
        if session_key == "?":
            sessions_list.append(parser.parse('20170101T000000'))
        else:
            sessions_list.append(p.from_time)
        
    if nb > 0:
        player_list[session_key][message.author] = p
    else:
        # No player is registered for this session, do nothing.
        if message.author not in player_list[session_key]:
            return None

        player_list[session_key][message.author].extra -= -nb
        player_list[session_key][message.author].declined += nb
        if player_list[session_key][message.author].extra <= -1:
            # Nobody's there anymore !
            del(player_list[session_key][message.author])

    answer = get_message(player_list, message)
    new_data = {"player_list": player_list, "sessions_list": sessions_list }
    save(message.channel.id, new_data)
    return answer

def get_message(player_list, message):
    answer = 'Players list :\n\n'

    for session in player_list:
        player_lvs = {}
        
        if len(player_list[session]) == 0:
            continue
    
        answer = answer + '**[' + session + ']**\n'
        available_players = player_list[session]

        player_roles = { "Unknown": 0, "Not sure": 0 }
        count = 0
        for key in available_players:
            p = available_players[key]

            print(p)
            
            player_count = 1 + p.extra
            count += 1
            player_name = '#'.join(str(key).split('#')[:-1])
            
            answer += '> **{0}** ({1}) '.format(player_name, rolestostring(p.roles))
            if player_count > 1:
                answer += ' [+{0}]'.format(player_count - 1)
                count += player_count - 1
                if p.declined == 0:
                    player_roles["Unknown"] += player_count - 1
                
            # Player roles
            if p.declined == 0:
                for role in p.roles:
                    if role.name in player_roles:
                        player_roles[role.name] += 1
                    else:
                        player_roles[role.name] = 1

                # Player levels, only count if number of levels registered = number of players registered
                # and no one declined afterwards
                if len(p.lvs) == player_count:
                    for lv in p.lvs:
                        if lv not in player_lvs:
                            player_lvs[lv] = 1
                        else:
                            player_lvs[lv] += 1
            else:
                player_roles["Not sure"] += player_count

        answer += '\nTOTAL : **{0}** - '.format(count)        

        # Player roles
        for r in player_roles:
            if player_roles[r] == 0 and (r == 'Unknown' or r == 'Not sure'):
                continue
            answer += '{0} : {1}   '.format(r, player_roles[r])

        # Player levels
        answer += '\nPlayer levels : '
        for l in player_lvs:
            answer += '{0} x L{1}  '.format(player_lvs[l], l)

        # Line break (end of player list for this session)
        answer += '\n\n'

    # Disclaimer
    answer += '---\n'
    answer += "I'm still learning so I may have missed some messages, if so please ping @tama\n"
    answer += 'Source code (pull requests welcome) : <https://bitbucket.org/tamati25/pokemon-go-discord-raid-rsvp-bot>'
    return answer

def save(filename, data):
    pickle.dump(data, open(filename, 'wb'))

def load(filename):
    return pickle.load(open(filename, 'rb'))

def rolestostring(roles):
    ret = ''
    for role in roles:
        ret = ret + ',' + role.name
    return ret[1:]

if __name__ == '__main__':
    token = get_token()
    if token is not None:
        client.run(token)
